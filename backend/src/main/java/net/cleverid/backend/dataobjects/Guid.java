/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.cleverid.backend.dataobjects;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.FilterOperator;
import java.util.UUID;

/**
 *
 * @author denz
 */
@Persistent
public class Guid extends DataObject {
	@Persistent
	private String email;
	/*
	@KeyProperty
	@Persistent
	private int id;
	*/
	@KeyProperty
	@Persistent
	private String guid;

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * @param guid the guid to set
	 */
	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String generate() {
		String myGuid = ((UUID.randomUUID()+"").replace("-", ""));
		this.guid = myGuid;
		return guid;
	}
	public static UserData getUserData(String guid) {
		Filter filter = new FilterPredicate("guid", FilterOperator.EQUAL, guid);
		Iterable<Entity> entities = Guid.filterBy(Guid.class, filter);
		for (Entity entity : entities) {
			Guid guidObj = new Guid();
			guidObj.setFields(entity);
			String email = guidObj.getEmail();
			UserData userData = UserData.get(email);
			//userData.setFields(entity);
			guidObj.delete();
			return userData;
		}
		return null;
	}
}