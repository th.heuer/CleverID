/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cleverid.backend.dataobjects;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author @geekdenz
 */
public class DataObject {

	private Entity myEntity;

	public void save() {
		String className = this.getClass().getCanonicalName();
		Map<String, String> keyStrings = this.getKeyStrings();
		Entity existingEntity = getExistingEntity(keyStrings);
		if (existingEntity == null) {
			Key myKey = KeyFactory.createKey(className, this.hashCode());
			myEntity = new Entity(className, myKey);
		} else {
			myEntity = existingEntity;
		}
		Date date = new Date();
		myEntity.setProperty("Changed", date);
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

		Class<? extends DataObject> thisClass = this.getClass();
		Annotation[] annotations = thisClass.getAnnotations();
		try {
			for (Annotation annotation : annotations) {
				if (annotation instanceof Persistent) {
					saveProperties(thisClass);
					break;
				}
			}
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		}
		datastore.put(myEntity);
	}

	private void saveProperties(Class<? extends DataObject> thisClass) throws IllegalArgumentException, IllegalAccessException {
		Field[] fields = thisClass.getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);
			Annotation[] fieldAnnotations = field.getAnnotations();
			for (Annotation fieldAnnotation : fieldAnnotations) {
				if (fieldAnnotation instanceof Persistent) {
					myEntity.setProperty(field.getName(), field.get(this));
				}
			}
		}
	}

	public static Iterable<Entity> filterBy(Class clazz, Filter filter) {
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Query q = new Query(clazz.getCanonicalName()).setFilter(filter);
		PreparedQuery pq = datastore.prepare(q);
		return pq.asIterable();
	}

	public void setFields(Entity entity) {
		Field[] fields = this.getClass().getDeclaredFields();
		try {

			for (Field field : fields) {
				field.setAccessible(true);
				String methodName = "set"
						+ field.getName().substring(0, 1).toUpperCase()
						+ field.getName().substring(1);
				Object prop = entity.getProperty(field.getName());
				if (prop == null) {
					continue;
				}
				Class type = prop.getClass();
				Method method = this.getClass().getDeclaredMethod(methodName, type);
				method.invoke(this, entity.getProperty(field.getName()));
			}
		} catch (NoSuchMethodException ex) {
			throw new RuntimeException(ex);
		} catch (IllegalAccessException ex) {
			throw new RuntimeException(ex);
		} catch (IllegalArgumentException ex) {
			throw new RuntimeException(ex);
		} catch (InvocationTargetException ex) {
			throw new RuntimeException(ex);
		}
	}

	private Map<String, String> getKeyStrings() {
		Field[] fields = this.getClass().getDeclaredFields();
		Map<String, String> keyStrings = new HashMap<String, String>();
		try {
			for (Field field : fields) {
				field.setAccessible(true);
				Annotation[] fieldAnnotations = field.getAnnotations();
				for (Annotation fieldAnnotation : fieldAnnotations) {
					if (fieldAnnotation instanceof KeyProperty) {
						keyStrings.put(field.getName(),
								field.get(this).toString());
					}
				}
			}
		} catch (IllegalArgumentException ex) {
			throw new RuntimeException(ex);
		} catch (IllegalAccessException ex) {
			throw new RuntimeException(ex);
		}
		return keyStrings;
	}

	private Entity getExistingEntity(Map<String, String> keyStrings) {
		List<Filter> filters = new ArrayList<Filter>(keyStrings.size());
		for (Map.Entry<String, String> entry : keyStrings.entrySet()) {
			Filter filter = new Query.FilterPredicate(entry.getKey(), Query.FilterOperator.EQUAL, entry.getValue());
			filters.add(filter);
		}
		Filter composite = null;
		if (filters.size() > 1) {
			composite = new Query.CompositeFilter(
				Query.CompositeFilterOperator.AND, filters);
		} else {
			composite = filters.get(0);
		}
		Iterable<Entity> entities = filterBy(this.getClass(), composite);
		for (Entity entity : entities) {
			return entity;
		}
		return null;
	}
	protected Entity getExisting() {
		String className = this.getClass().getCanonicalName();
		Map<String, String> keyStrings = this.getKeyStrings();
		Entity existingEntity = getExistingEntity(keyStrings);
		return existingEntity;
	}
	public void delete() {
		Entity existing = getExisting();
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		datastore.delete(existing.getKey());
	}
}
