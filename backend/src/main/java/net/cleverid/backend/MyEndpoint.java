/*
   For step-by-step instructions on connecting your Android application to this backend module,
   see "App Engine Java Endpoints Module" template documentation at
   https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloEndpoints
*/

package net.cleverid.backend;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Nullable;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.users.User;

import net.cleverid.backend.dataobjects.Guid;
import net.cleverid.backend.dataobjects.UserData;
import net.cleverid.backend.response.IdResponse;
import net.cleverid.backend.response.IdsResponse;
import net.cleverid.backend.response.SuccessResponse;
import net.cleverid.backend.response.UserDataResponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Named;

/**
 * An endpoint class we are exposing
 * Explore at http://localhost:8080/_ah/api/explorer
 */
@Api(
        name = "cleverid", version = "v1.3",
        namespace = @ApiNamespace(
                ownerDomain = "backend.cleverid.net",
                ownerName = "backend.cleverid.net",
                packagePath = ""
        ),
	description = "A clever API for clever people.",
	scopes = {ClientIds.EMAIL_SCOPE},
	clientIds = {
            ClientIds.WEB_CLIENT_ID,
            ClientIds.ANDROID_CLIENT_ID,
            ClientIds.IOS_CLIENT_ID,
            ClientIds.API_EXPLORER_CLIENT_ID
    },
	audiences = {ClientIds.ANDROID_AUDIENCE}
)
public class MyEndpoint {

	private static int MAX_IDS = 20;

	// Make sure to add this endpoint to your web.xml file if this is a web application.

    // added an empty constructor...
	public MyEndpoint() { };


	/**
     * A simple endpoint method that takes a name and says Hi back
     */
    @ApiMethod(name = "sayHi")
    public MyBean sayHi(@Named("name") String name) {
        MyBean response = new MyBean();
        response.setData("Hi, " + name);

        return response;
    }

    
	@ApiMethod(
		name = "postUserData",
		httpMethod = "post",
		path = "postUserData"
	)
	public SuccessResponse postUserData(User user,
			@Named("fullName") String fullName,
			//@Named("email") @Nullable String email,
			@Named("dateOfBirth") Date dateOfBirth,
			@Named("address1") String address1,
			@Named("address2") @Nullable String address2,
			@Named("city") String city,
			@Named("postcode") @Nullable String postcode,
			@Named("country") String country)
			throws UnauthorizedException {
		if (user == null) {
			throw new UnauthorizedException("Authorization required");
		}
		String email = user.getEmail();
		UserData userData = new UserData(user);
		userData.setFullName(fullName);
		userData.setEmail(email);
		userData.setDateOfBirth(dateOfBirth);
		userData.setAddress1(address1);
		userData.setAddress2(address2);
		userData.setCity(city);
		userData.setPostcode(postcode);
		userData.setCountry(country);
		userData.save();
		return new SuccessResponse();
	}
	protected String getGuid(User user) {
		Guid guid = new Guid();
		guid.setEmail(user.getEmail());
		guid.generate();
		guid.save();
		return guid.getGuid();
	}
	@ApiMethod(
		name = "getId",
		httpMethod = "get",
		path = "getId"
	)
	public IdResponse getId(User user) throws UnauthorizedException {
		UserData userData = getUserData(user);
		IdResponse response = new IdResponse();
		response.setId(getGuid(user));
		return response;
	}

	@ApiMethod(
			name = "getIds",
			httpMethod = "get",
			path = "getIds"
	)
	public IdsResponse getIds(User user, @Named("amount") int amount) throws UnauthorizedException {
		UserData userData = getUserData(user);
		if (amount > MAX_IDS) {
			throw new UnauthorizedException("More than "+ MAX_IDS +
					" requested. Use only up to "+ MAX_IDS +" as amount");
		}
		List<String> guids = new ArrayList<>(amount);
		for (int i = 0; i < amount; ++i) {
			guids.add(getGuid(user));
		}
		IdsResponse response = new IdsResponse(guids);
		return response;
	}

	protected UserData getUserData(User user) throws UnauthorizedException {
		if (user == null) {
			throw new UnauthorizedException("Authorization required");
		}
		String email = user.getEmail();
		UserData userData = UserData.get(email);
		if (userData == null) {
			throw new UnauthorizedException("Registration required");
		}
		return userData;
	}

	@ApiMethod(
		name = "simple",
		httpMethod = "get",
		path = "simple"
	)
	public IdResponse simple() {
		IdResponse idr = new IdResponse();
		idr.setId("asdauolhsduhasud");
		return idr;
	}
	@ApiMethod(
		name = "getUserInfo",
		httpMethod = "get",
		path = "getUserInfo/{guid}"
	)
	public UserDataResponse getUserInfo(User user, @Named("guid") String guid)
			throws UnauthorizedException {
		if (user == null) {
			throw new UnauthorizedException("Authorization required.");
		}

		UserData userData = Guid.getUserData(guid);
		if (userData == null) {
			throw new UnauthorizedException("Wrong GUID. Valid code required.");
		}
		UserDataResponse response = new UserDataResponse();
		setUserData(response, userData);
		return response;
	}

	private void setUserData(UserDataResponse response, UserData userData) {
		response.setAddress1(userData.getAddress1());
		response.setAddress2(userData.getAddress2());
		response.setCity(userData.getCity());
		response.setCountry(userData.getCountry());
		response.setDriversLicenseNumber(userData.getDriversLicenseNumber());
		response.setEmail(userData.getEmail());
		response.setFullName(userData.getFullName());
		response.setPostcode(userData.getPostcode());
		response.setDateOfBirth(userData.getDateOfBirth());
	}
	/*
	@ApiMethod(
		name = "cleverid.postImage",
		httpMethod = "post",
		path = "cleverid/postImage"
	)
	public SuccessResponse postImage(@Named("file") file) {
		SuccessResponse response = new SuccessResponse();
		response.setSuccess("OK");
		return response;
	}
	*/
}
