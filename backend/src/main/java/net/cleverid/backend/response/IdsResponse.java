package net.cleverid.backend.response;

import java.util.List;

/**
 * Created by denz on 7/22/15.
 */
public class IdsResponse {
    private List<String> ids;
    public IdsResponse(List<String> ids) {
        this.ids = ids;
    }
    public List<String> getIds() {
        return ids;
    }
}
