package net.cleverid.android;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;


public class GetProfileActivity extends ActionBarActivity {

    // dialog for progress bar message
    private ProgressDialog dialog;

    static final int GET_PROFILE_REQUEST = 1080;   // more or less random id for check onActivityResult
    // debug log TAG
    private final String TAG = GetProfileActivity.class.getSimpleName();
    // token to acquire to access endpoints
    private String oauth2Token = null;
    private String mGUID;
    private String mContents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_profile);

        // check started with intent from ToDo: somewhere!!!
        Intent intent = getIntent();
        String contents = intent.getStringExtra("scan_result");

        Log.v(TAG,"\n" + "contents of string Extra to be used as GUID ID ... " + contents + "\n");

        // the contents of the scamn result should be a GUID
        mGUID = contents;
        // the contents can also be used to read the contents of the scan even if its not a GUID or QR Code
        mContents = contents;

        // make sure we have 1 google user
        // and they get a valid oauth2 token...
        Intent AccessTokenIntent = new Intent(getApplicationContext(), RetrieveAccessTokenActivity.class);
        startActivityForResult(AccessTokenIntent, GET_PROFILE_REQUEST);

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_get_profile, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == GET_PROFILE_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {

                Bundle res = data.getExtras();
                oauth2Token = res.getString("token");
                Log.d("\n", "token: " + oauth2Token + "\n");

                //
                //new profileAsyncHttpTask().execute();
            }
        }
    }

    // want 1 async task that gets called to GET id code and look at response
    public class profileAsyncHttpTask extends AsyncTask<String, Void, Integer> {

        String response = null;
        JSONObject mainObject =null;
        String profile_email = null;
        String profile_firstName = null;
        String profile_lastName = null;
        String profile_dateOfBirth = null;
        String profile_address1 = null;
        String profile_city = null;
        String profile_country = null;
        String profile_kind = null;


        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(GetProfileActivity.this);
            dialog.setTitle("Working on downloading user profile information for the owner of this code...");
            dialog.setMessage("Please wait...");
            dialog.setIndeterminate(true);
            dialog.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            InputStream inputStream = null;
            HttpsURLConnection urlConnection = null;
            int statusCode = 0;


            try {
            /* forming the java.net.URL object */
                URL url = new URL(EndPointServerURL.cleveridclient + "getUserInfo" + "/" + mGUID);

                urlConnection = (HttpsURLConnection) url.openConnection();

            /* optional request header */
                urlConnection.setRequestProperty("Content-Type", "application/json");
            /* optional request header */
                urlConnection.setRequestProperty("Authorization", "Bearer " + oauth2Token);
            /* for Get request */
                urlConnection.setRequestMethod("GET");
                statusCode = urlConnection.getResponseCode();
                Log.v(TAG, "Status code..." + statusCode + "\n");
                Log.v(TAG, "\n" + "the email scope token is... " + oauth2Token + "\n");

            /* 200 represents HTTP OK */
                if (statusCode == 200) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                    response = convertInputStreamToString(inputStream);

                    Log.v(TAG, "\n" + "Response: " + response + "\n");
                    try {

                        mainObject = new JSONObject(response);

                        profile_email = mainObject.getString("email");
                        profile_firstName = mainObject.getString("firstName");
                        profile_lastName = mainObject.getString("lastName");
                        profile_dateOfBirth = mainObject.getString("dateOfBirth");
                        profile_address1 = mainObject.getString("address1");
                        profile_city = mainObject.getString("city");
                        profile_country = mainObject.getString("country");
                        profile_kind = mainObject.getString("kind");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    // for debugging I want to log the response here eg. 401
                    response = "0"; //"Failed to fetch data!";
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }

            return statusCode; //"Failed to fetch data!";

        } // end of doInBackground

        @Override
        protected void onPostExecute(Integer result) {
        /* Download complete. Lets do something with it... */
            // used startActivityForResult to launch your child Activity, ScanActivity
            Intent profileResultIntent = new Intent();

            if (result == 200) {


                //  Add extras or a data URI to this intent
                profileResultIntent.putExtra("email_result", profile_email);
                profileResultIntent.putExtra("firstName_result", profile_firstName);
                profileResultIntent.putExtra("lastName_result", profile_lastName);
                profileResultIntent.putExtra("dateOfBirth_result", profile_dateOfBirth);
                profileResultIntent.putExtra("address1_result", profile_address1);
                profileResultIntent.putExtra("city_result", profile_city);
                profileResultIntent.putExtra("country_result", profile_country);
                profileResultIntent.putExtra("kind_result", profile_kind);
                profileResultIntent.putExtra("status_result", result);

                setResult(RESULT_OK, profileResultIntent);
                finish();


            } else {
                Log.e(TAG, "Failed!");

                profileResultIntent.putExtra("kind_result", "ERROR");
                profileResultIntent.putExtra("status_result", result);
                profileResultIntent.putExtra("scan_result", mContents);
                setResult(RESULT_OK, profileResultIntent);
                finish();

            }


            dialog.dismiss();
            finish();

        }


        private String convertInputStreamToString(InputStream inputStream) throws IOException {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while ((line = bufferedReader.readLine()) != null) {
                result += line;

            }

        /* Close Stream */
            if (null != inputStream) {
                inputStream.close();
            }
            return result;
        }

    } //end of class profileAsyncHttpTask



} // end of GetProfileActivity
