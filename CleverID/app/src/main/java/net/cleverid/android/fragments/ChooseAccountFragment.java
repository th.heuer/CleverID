package net.cleverid.android.fragments;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.api.client.googleapis.extensions.android.accounts.GoogleAccountManager;

import net.cleverid.android.Preferences;
import net.cleverid.android.R;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Created by denz on 10/01/16.
 */
public class ChooseAccountFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private Account[] accounts;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment 1st
        View v = inflater.inflate(R.layout.fragment_choose_account, container, false);

        Spinner chooseAccountSpinner = (Spinner) v.findViewById(R.id.spinner_choose_account);

        String[] accountNames = getAccountNames();
        String defaultAccountName = accountNames[0];
        String name = Preferences.get(Preferences.ACCOUNT, defaultAccountName);
        int position = Arrays.asList(accountNames).indexOf(name);

        ArrayAdapter<String> accounts = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, accountNames);
        accounts.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        chooseAccountSpinner.setAdapter(accounts);
        //chooseAccountSpinner.setOnItemClickListener(this);
        chooseAccountSpinner.setOnItemSelectedListener(this);
        chooseAccountSpinner.setSelection(position);
        return v;
    }

    private String[] getAccountNames() {
        AccountManager manager = AccountManager.get(this.getActivity());
        GoogleAccountManager am = new GoogleAccountManager(manager);
        accounts = am.getAccounts();
        String[] names = new String[accounts.length];
        int i = 0;
        for (Account account : accounts) {
            names[i++] = account.name;
        }
        return names;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Preferences.set(Preferences.ACCOUNT, accounts[position].name);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
