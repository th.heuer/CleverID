package net.cleverid.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;



import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class ScanActivity extends ActionBarActivity {

    private final String TAG = ScanActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_scan);

        Context context = this;
        LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // Inflate the layout for this activity
        View v = inflater.inflate(R.layout.activity_scan, null);

        // null means we're doin a new instance, not recreating a previously destroyed instance
        if (savedInstanceState == null) {

            // when we're ready to start scanning
            Log.v(TAG, "ready to scan...");
            IntentIntegrator integrator = new IntentIntegrator(this);

            integrator.setPrompt("Scan a code");
            integrator.initiateScan();

            //        integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
            //        integrator.setPrompt("Scan a barcode");
            //        integrator.setResultDisplayDuration(0);
            //        integrator.setWide();  // Wide scanning rectangle, may work better for 1D barcodes
            //        integrator.setCameraId(0);  // Use a specific camera of the device
            //        integrator.initiateScan();

        }
    }

    // When zxing has finished scanning...
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {

            // handle scan result
            Log.v(TAG, "we have a scan result: " + resultCode);

            // if scan result = 0 probably a back button pressed or cancel for some other reason?
            if (resultCode == 0) {

                //dialog.dismiss();
                finish();


            } else {

                // yay, we got a real result...(not sure about checking resultCode == RESULT_OK)
                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

                Log.v(TAG, "contents: " + contents);
                Log.v(TAG, "format: " + format);

                //mScanTextView.setText("This is the scan result: " + contents);

                // go to a new activity to POST contents as id
                // and get a user profile / email account
                // note launch activity from fragment
                // Intent getProfileIntent = new Intent(getActivity(), GetProfileActivity.class);
                // getProfileIntent.putExtra("SCAN_RESULT",contents);
                // startActivity(getProfileIntent);

                // used startActivityForResult to launch your child Activity, ScanActivity
                Intent resultIntent = new Intent();
                //  Add extras or a data URI to this intent
                resultIntent.putExtra("scan_result", contents);

                setResult(RESULT_OK, resultIntent);
                finish();


            }

        } else {
            // else continue with any other code you need in the method
            Log.v(TAG, "problem getting a scan result: " + resultCode);
            finish();
        }

        // not sure what to do now...
        Log.v(TAG, "OK we're at the end of the scan activity so we can definitely finish now!!! " + "\n");

        //dialog.dismiss();
        finish();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_scan, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
