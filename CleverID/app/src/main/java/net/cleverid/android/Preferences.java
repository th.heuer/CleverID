package net.cleverid.android;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {
    public final static String ACCOUNT = "ACCOUNT";
    public static Context context = null;
    private static SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(
                Constants.PRIVATE_PREFERENCES,
                Context.MODE_PRIVATE);
    }
    public static String get(String name, String defaultValue) {
        return getSharedPreferences().getString(name, defaultValue);
    }
    public static void set(String key, String value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(key, value);
        editor.commit();
    }
    public static int getInt(String name, int defaultValue) {
        return Integer.parseInt(getSharedPreferences().getString(name, ""+ defaultValue));
    }
}
