package net.cleverid.android;

import android.content.Context;
import android.content.Intent;
import android.preference.DialogPreference;
import android.util.AttributeSet;

import com.google.android.gms.common.AccountPicker;

public class ChooseAccountPreference extends DialogPreference {
    public static final String TAG = ChooseAccountPreference.class.getName();

    public ChooseAccountPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context,attrs,defStyleAttr);
    }
    public ChooseAccountPreference(Context context, AttributeSet attrs) {
        super(context,attrs,0);
    }
    @Override
    public void onClick() {
        pickUserAccount();
    }
    private void pickUserAccount() {
        String[] accountTypes = new String[]{"com.google"};
        Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                accountTypes, false, null, null, null, null);
        SettingsActivity settingsActivity = (SettingsActivity) getContext();
        settingsActivity.startActivityForResult(intent, SettingsActivity.REQUEST_CODE_PICK_ACCOUNT);
    }
}
