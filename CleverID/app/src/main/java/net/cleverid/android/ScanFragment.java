package net.cleverid.android;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ScanFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ScanFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ScanFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER need 8
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final String ARG_PARAM5 = "param5";
    private static final String ARG_PARAM6 = "param6";
    private static final String ARG_PARAM7 = "param7";
    private static final String ARG_PARAM8 = "param8";

    // TODO: Rename and change types of parameters
    private String mEmail; //mParam1;
    private String mFirstName; //mParam2;
    private String mLastName;
    private String mDateOfBirth;
    private String mAddress1;
    private String mCity;
    private String mCountry;
    private String mKind;
    private String mStatus;

    private OnFragmentInteractionListener mListener;

    private final String TAG = ScanFragment.class.getSimpleName();
    private TextView mScanTextView;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ScanFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ScanFragment newInstance(String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8 ) {
        ScanFragment fragment = new ScanFragment();

        Bundle args = new Bundle();
        if (param1.equals("ERROR"))  {
            args.putString(ARG_PARAM1,"ERROR");
            args.putString(ARG_PARAM2, param2);
            args.putString(ARG_PARAM3, param3);
        }else{
            args.putString(ARG_PARAM1, param1);
            args.putString(ARG_PARAM2, param2);
            args.putString(ARG_PARAM3, param3);
            args.putString(ARG_PARAM4, param4);
            args.putString(ARG_PARAM5, param5);
            args.putString(ARG_PARAM6, param6);
            args.putString(ARG_PARAM7, param7);
            args.putString(ARG_PARAM8, param8);
        }

        fragment.setArguments(args);
        return fragment;
    }

    public ScanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            if (getArguments().getString(ARG_PARAM1).equals("ERROR")) {

                // not setting any members

            }else{

                mEmail = getArguments().getString(ARG_PARAM1);
                mFirstName = getArguments().getString(ARG_PARAM2);
                mLastName = getArguments().getString(ARG_PARAM3);
                mDateOfBirth = getArguments().getString(ARG_PARAM4);
                mAddress1 = getArguments().getString(ARG_PARAM5);
                mCity = getArguments().getString(ARG_PARAM6);
                mCountry = getArguments().getString(ARG_PARAM7);
                mKind = getArguments().getString(ARG_PARAM8);

            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_scan, container, false);

        mScanTextView = (TextView) v.findViewById(R.id.scan_text);

            if (getArguments().getString(ARG_PARAM1).equals("ERROR")) {

                mScanTextView.setText("There was a problem with the scan: " + "\n\n"
                        + "status: "+ (getArguments().getString(ARG_PARAM3)) + "\n\n"
                        + "this is what was scanned: " + (getArguments().getString(ARG_PARAM2)));


            }else{

                mScanTextView.setText("Scan Result matches this profile: " + "\n\n"
                        + "Email: " + mEmail + "\n"
                        + "First name: " + mFirstName + "\n"
                        + "Last name: " + mLastName + "\n"
                        + "Date of birth: " + mDateOfBirth + "\n"
                        + "Address: " + mAddress1 + "\n"
                        + "City: " + mCity + "\n"
                        + "Country: " + mCountry + "\n"
                        + "\n");
            }
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }



    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


}
