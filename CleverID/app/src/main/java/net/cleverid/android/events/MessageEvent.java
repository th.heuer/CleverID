package net.cleverid.android.events;

/**
 * Created by denz on 12/2/15.
 */
public class MessageEvent {
    public final String message;

    public MessageEvent(String message) {
        this.message = message;
    }
}
