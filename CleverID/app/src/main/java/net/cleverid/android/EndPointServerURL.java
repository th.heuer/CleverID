package net.cleverid.android;

/**
 * Created by johney on 30/05/15.
 */
public class EndPointServerURL {

    //
    //public static final String cleveridclient = "https://cleveridclient.appspot.com/_ah/api/cleverid/v1.2/";
    public static final String cleveridclient = "https://cleveridme.appspot.com/_ah/api/cleverid/v1.3/";
    public static final String cleverid = "https://cleveridme.appspot.com/_ah/api/";

}
