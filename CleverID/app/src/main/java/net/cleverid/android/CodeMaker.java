package net.cleverid.android;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.util.Hashtable;

/**
 * Created by denz on 1/2/16.
 */
public class CodeMaker {
    public final static int WHITE = 0xFFFFFFFF;
    public final static int BLACK = 0xFF000000;
    private static CodeMaker instance = new CodeMaker();
    private CodeMaker() {} // private constructor
    public static CodeMaker inst() {
        return instance;
    }
    public void setQrCode(ImageView imageView, String code) throws WriterException {
        int width = (int) imageView.getRootView().getResources().getDimension(R.dimen.image_width);
        int height = (int) imageView.getRootView().getResources().getDimension(R.dimen.image_height);
        setQrCode(imageView, code, Math.min(width, height));
    }
    public void setQrCode(ImageView imageView, String code, int width) throws WriterException {
        imageView.setImageBitmap(encodeAsBitmap(code, width));
    }
    private Bitmap encodeAsBitmap(String str, int width) throws WriterException {
        BitMatrix result;
        try {
            Hashtable<EncodeHintType, ErrorCorrectionLevel> hints = new Hashtable<>();
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, width, width, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, w, h);
        return bitmap;
    }
}
