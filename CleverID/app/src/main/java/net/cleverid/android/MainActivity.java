package net.cleverid.android;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import net.cleverid.android.events.MessageEvent;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import de.greenrobot.event.EventBus;

public class MainActivity extends ActionBarActivity implements
        TopFragment.OnFragmentInteractionListener,
        QRFragment.OnFragmentInteractionListener,
        ScanFragment.OnFragmentInteractionListener {

    private static final long QR_REFRESH = 5;
    // this is a debug tag for Log
    private final String TAG = MainActivity.class.getSimpleName();

    // The top fragment is only created once, see onCreate
    private final TopFragment mTopFragment = new TopFragment();

    // This is a initial default/test GUID that will be displayed as a QR code
    private String qrGuid = "Hello! You should login to CleverID and download a real ID";

    // a request code to ensure the scan activity responds correctly
    static final int SCAN_ACTIVITY_REQUEST = 1078;
    static final int GET_PROFILE_ACTIVITY_REQUEST = 1978;
    static final int GET_GUID = 2078;
    private Intent mServiceIntent = null;
    private static ScheduledExecutorService scheduler = null;
    private static ScheduledFuture handle = null;
    private Counter counter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // creating a view for
        Context context = this;
        LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.activity_main, null);

        // normal way of setting a view
        setContentView(R.layout.activity_main);

        // null means we're doin a new instance, not recreating a previously destroyed instance
        if (savedInstanceState == null) {

        // Well, lets get started by adding a TopFragment instance to fragment_main.xml

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            mTopFragment.setArguments(getIntent().getExtras());

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            // Add the fragment to the 'fragment_container' FrameLayout
            fragmentTransaction
                    .add(R.id.fragment_main_1, mTopFragment);
            fragmentTransaction
                    .commit();

            // if there's a valid GUID stored, display that,
            // updateQR starts QRFragment, fragment retrieve GUIDs from the private file and displays them
            //updateQR();


        }
        Preferences.context = this;

        startOauthAndGuidGettingProcess();

        GuidModel guidModel = new GuidModel(this);
        qrGuid = guidModel.getNextGuid();
        updateQR(qrGuid);
    }
    public void startOauthAndGuidGettingProcess() {
        String email = Preferences.get("account", null);
        if (email != null) {
            (new Oauth2TokenTask(this)).execute(email);
        }
    }

    public void takePhoto(View view) {
        Intent intent = new Intent(this, TakePhotoActivity.class);
        startActivity(intent);
    }

    public void refresh(View view) {
        //getNextGuid();
        Intent intent = new Intent(this, MainMaterialActivity.class);
        this.startActivity(intent);
    }

    private static class Counter {
        public int count = 0;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            this.startActivity(settingsIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    // Some interaction...

    //This gets called from a fragment (eg. TopFragment) onClick with a mListener.onFragmentInteraction(null);
    @Override
    public void onFragmentInteraction(Uri uri) {
        // when fragment gets attached to the activity...
        // and clicked
        Toast toast = Toast.makeText(this, "got communication from the fragment!", Toast.LENGTH_SHORT);
        toast.show();

    }

    // these methods are called from the buttons own button:onClick defined in fragment.xml, eg fragment_top.xml

    // get a new user logged into and registered with the system
    public void getLogin(View v) {

        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }



    // debug method for viewing our tokens if needed
//    public void getToken(View v) {
//
//        Intent intent = new Intent(this, RetrieveAccessTokenActivity.class);
//        startActivity(intent);
//
//    }


    // updateQR starts QRFragment, fragment retrieves GUIDs from the private file and displays them
    // its a bit more complicated, create and load a new fragment (with a (new) QR Code inside)

    private QRFragment newQRFragment = null;
    public void updateQR(String guid) {
        Date date = new Date();
        Log.v(TAG, "Date: "+ date.toString());
        Log.v(TAG, "GUID = "+ guid);

        // First up, you would have a GUID, qrGuid to use to gen the Qr from here

        //QRFragment newQRFragment = new QRFragment();
        // oh yeah, we need to pass in a GUID to show it!
        //QRFragment newQRFragment = QRFragment.newInstance("ARG_PARAM1", qrGuid);
        newQRFragment = QRFragment.newInstance(guid, "another val for later?");
        newQRFragment.setGuid(guid);
        FragmentManager fragmentManager2 = getFragmentManager();
        FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();

        // Replace the fragment in the 'fragment_container' FrameLayout in activity_main.xml
        fragmentTransaction2
                .replace(R.id.fragment_main_2, newQRFragment);
        fragmentTransaction2
                .commit();



        // In case this activity was started with special instructions from an
        // Intent, pass the Intent's extras to the fragment as arguments

        // Don't need...
        //newQRFragment.setArguments(getIntent().getExtras());

        // its got to generate its own view, make the qr code



    }

    // start the scanning process
    public void scanCode(View v) {

        // get the zxing scanner scanning in ScanActivity...
        Intent scanActivityIntent = new Intent(getApplicationContext(), ScanActivity.class);
        startActivityForResult(scanActivityIntent, SCAN_ACTIVITY_REQUEST);

    }

    // the result of the scanning process...
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        ScanFragment newScanFragment;
        String scanResult = null;

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case GET_GUID:
                    handleGuid(data);
            }
        }
        // Check which request we're responding to
        if (requestCode == SCAN_ACTIVITY_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {

                Bundle res = data.getExtras();
                scanResult = res.getString("scan_result");
                Log.v(TAG, "\n" + "the scan result is: " + scanResult + "\n");

                // send scanResult to GetProfileActivity
                Intent getProfileActivityIntent = new Intent(getApplicationContext(), GetProfileActivity.class);
                getProfileActivityIntent.putExtra("scan_result", scanResult);
                startActivityForResult(getProfileActivityIntent, GET_PROFILE_ACTIVITY_REQUEST);

            }

            Log.e(TAG,"there was a problem getting a scan");
        }

        if (requestCode == GET_PROFILE_ACTIVITY_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {

                Bundle res = data.getExtras();

                //    // the result of the profile request...
                String status_result = Integer.toString(res.getInt("status_result"));
                Log.v(TAG, "\n" + "um,  the status_result is: " + status_result + "\n");
                scanResult = res.getString("scan_result");
                Log.v(TAG, "\n" + "um,  the scanResult is: " + scanResult + "\n");
                String kindResult = res.getString("kind_result");
                Log.v(TAG, "\n" + "um,  the kindResult is: " + kindResult + "\n");

                if (kindResult.equals("ERROR")) {
                    // pass vals to scan fragment for display - even if it's not a QR Code!
                    newScanFragment = ScanFragment.newInstance(kindResult, scanResult, status_result, null, null, null, null, null);
                    generateFragment(newScanFragment);


                } else {

                    String emailResult = res.getString("email_result");
                    Log.v(TAG, "\n" + "yes, we're ok, the profile result is: " + emailResult + "\n");

                    String firstNameResult = res.getString("firstName_result");
                    Log.v(TAG, "\n" + "yes, we're ok, the profile result is: " + firstNameResult + "\n");

                    String lastNameResult = res.getString("lastName_result");
                    Log.v(TAG, "\n" + "yes, we're ok, the profile result is: " + lastNameResult + "\n");

                    String dateOfBirthResult = res.getString("dateOfBirth_result");
                    Log.v(TAG, "\n" + "yes, we're ok, the profile result is: " + dateOfBirthResult + "\n");

                    String address1Result = res.getString("address1_result");
                    Log.v(TAG, "\n" + "yes, we're ok, the profile result is: " + address1Result + "\n");

                    String cityResult = res.getString("city_result");
                    Log.v(TAG, "\n" + "yes, we're ok, the profile result is: " + cityResult + "\n");

                    String countryResult = res.getString("country_result");
                    Log.v(TAG, "\n" + "yes, we're ok, the profile result is: " + countryResult + "\n");


                    // pass vals to scan fragment for display - even if it's not a QR Code!
                    newScanFragment = ScanFragment.newInstance(emailResult, firstNameResult, lastNameResult,
                            dateOfBirthResult, address1Result, cityResult, countryResult, kindResult);
                    generateFragment(newScanFragment);
                }
            }

            Log.e(TAG,"there was a problem getting the profile");
        }


    }

    private void handleGuid(Intent data) {
        Bundle extras = data.getExtras();
        String guid = extras.getString(GetGuidsActivity.GUID_RESULT);
        this.setQrGuid(guid);
    }

    // its got to generate its own view, show the profile details
    public void generateFragment(ScanFragment newScanFragment){

        FragmentManager fragmentManager2 = getFragmentManager();
        FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();



        // Replace the fragment in the 'fragment_container' FrameLayout in activity_main.xml
        fragmentTransaction2
                .replace(R.id.fragment_main_2, newScanFragment);

        fragmentTransaction2.addToBackStack(null);
        fragmentTransaction2
                .commit();


    }

    public void setQrGuid(String qrGuid) {
        this.qrGuid = qrGuid;
    }

    public void updateAuthToken(String oauth2Token) {
        Log.v(TAG, oauth2Token);
        GuidTaskControl control = new GuidTaskControl(this);
        control.setOauth2Token(oauth2Token);
        control.doUntilTokenInvalid();
    }


    // I don't think I'll use this placeholder fragment...

    /**
     * A placeholder fragment containing a simple view.
     */
//    public static class PlaceholderFragment extends Fragment {
//
//        public PlaceholderFragment() {
//        }
//
//        //GoogleAccountCredential credential;
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
//
//
//            return rootView;
//        }
//
//
//
//
//    } // end of PlaceholderFragment
//
//
//

    private static class MyState {
        public boolean oauthStarted = false;
    }
    final MainActivity that = this;
    final MyState myState = new MyState();
    Runnable updateQrRunnable = new Runnable() {
        @Override
        public void run() {
            getNextGuid();
        }
    };

    private void getNextGuid() {
        GuidModel model = new GuidModel(that);
        try {
            String guid = model.getNextGuid();
            if (!myState.oauthStarted && guid.equals("")) {
                startOauthAndGuidGettingProcess();
                myState.oauthStarted = true;
            }
            EventBus.getDefault().post(new MessageEvent(guid));
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        startGettingGuids();
    }

    private void startGettingGuids() {
        EventBus.getDefault().register(this);

        if (scheduler != null) {
            handle.cancel(false);
        }
        scheduler = Executors.newScheduledThreadPool(1);
        int updateFreq = Integer.parseInt(getSharedPreferences(Constants.PRIVATE_PREFERENCES,
                Context.MODE_PRIVATE).getString("pref_updateFreq", "5"));
        handle = scheduler.scheduleAtFixedRate(updateQrRunnable, 0, updateFreq, TimeUnit.SECONDS);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    // This method will be called when a MessageEvent is posted
    public void onEvent(MessageEvent event){
        //Toast.makeText(this, event.message, Toast.LENGTH_SHORT).show();

        updateQR(event.message);
        Log.v(TAG, "Event "+ event.message);
    }

} // end of MainActivity
