package net.cleverid.android;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;

import net.cleverid.backend.cleverid.Cleverid;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

//import com.securepreferences.SecurePreferences;
//import java.util.Collection;
//import java.util.Iterator;
//import java.util.List;
//import java.util.ListIterator;

/*
    Retrieves and writes GUIDs to a file

 */
public class GetGuidsActivity extends ActionBarActivity {

    public static final String PREF_ACCOUNT_NAME = "PREF_ACCOUNT_NAME";
    public static final String GUID_RESULT = "GUID_RESULT";
    private static final String TAG = GetGuidsActivity.class.toString();
    private ProgressDialog dialog;

    private static final String httpTAG = "RetrieveStuffFromHTTP";

    static final int OAUTH2_TOKEN_REQUEST = 1079;   // more or less random id for check onActivityResult

    // token to acquire to access endpoints
    private String oauth2Token = null;

    // temporary array od GUIDs from the server endpoint
    private ArrayList<String> someGuids = new ArrayList<>();

    // private (to the app) store file to store some GUIDs untill the app is uninstalled!
    private String FILENAME = "guidsFileSecured";
    private Cleverid.Builder builder;
    private SharedPreferences settings;
    private String accountName;
    private GoogleAccountCredential credential;
    private Cleverid service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_guids);

        // Getting guids, 1st step is to make sure we have 1 google user
        // and they get a valid oauth2 token...
        Intent AccessTokenIntent = new Intent(getApplicationContext(), RetrieveAccessTokenActivity.class);
        startActivityForResult(AccessTokenIntent, OAUTH2_TOKEN_REQUEST);
        //settings = getSharedPreferences("CleverIDAccount", 0);
        //Collection<String> scopes = new ArrayList<>();
        //String googleScope = "oauth2:https://www.googleapis.com/auth/userinfo.email";
        //scopes.add(googleScope);

        //credential = GoogleAccountCredential.usingOAuth2(this, scopes);
        //credential = GoogleAccountCredential.usingAudience(this,
                //"511690246358-2g8qmtluhhg7e83bpo1mt1ilu0mu6hfh.apps.googleusercontent.com");
        //"server:client_id:511690246358-2g8qmtluhhg7e83bpo1mt1ilu0mu6hfh.apps.googleusercontent.com");
        //setSelectedAccountName(settings.getString(PREF_ACCOUNT_NAME, null));
        /*
        builder = new Cleverid.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), credential);
                //.setRootUrl(EndPointServerURL.cleverid);
        service = builder.build();
        if (credential.getSelectedAccountName() != null) {
            // Already signed in, begin app!
        } else {
            // Not signed in, show login window or request an account.
        }
        */
        //EndpointsAsyncTask.credential = credential;
        //new EndpointsAsyncTask().execute(new Pair<Context, String>(this, "Tim"));
        /*
        try {
            Cleverid.GetId getId = service.getId();
            IdResponse getIdExe = getId.execute();
            String guid = getIdExe.getId().toString();
            Toast.makeText(this, guid, Toast.LENGTH_LONG);
        } catch (IOException e) {
            Toast.makeText(this, "Sorry Not Working", Toast.LENGTH_LONG).show();
        }
        */
    }

    // setSelectedAccountName definition
    private void setSelectedAccountName(String accountName) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREF_ACCOUNT_NAME, accountName);
        editor.commit();
        credential.setSelectedAccountName(accountName);
        this.accountName = accountName;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_get_guids, menu);


        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == OAUTH2_TOKEN_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {

                Bundle res = data.getExtras();
                oauth2Token = res.getString("token");
                Log.d("\n", "token: "+ oauth2Token + "\n");
                EndpointsAsyncTask.credential = new GoogleCredential().setAccessToken(oauth2Token);
                new EndpointsAsyncTask().execute(new Pair<Context, String>(this, "Tim"));

                GuidService.credential = new GoogleCredential().setAccessToken(oauth2Token);
                Intent mServiceIntent = new Intent(this, GuidService.class);
                //getApplication().startActivity(mServiceIntent);
                this.startService(mServiceIntent);
                //new AsyncHttpTask().execute();
            }
        }
    }

    public void setGuids(List<String> guids) {
        /*
        SharedPreferences prefs = new SecurePreferences(getApplicationContext(), "password", FILENAME);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putStringSet("guids", new HashSet<String>(guids));
        */
        /*
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(Constants.GUID_FILENAME, Context.MODE_PRIVATE);
            for (String guid : guids) {
                fos.write((guid + "\n").getBytes());
            }
        } catch (FileNotFoundException e) {
            Log.e(TAG, e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    Log.e(TAG, e.getMessage());
                }
            }
        }
        */
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }
    /*
    public void setGuid(String result) {
        SharedPreferences prefs = new SecurePreferences(getApplicationContext(), "password", FILENAME);

        SharedPreferences.Editor editor = prefs.edit();
            editor.putString("id",result).apply();
            Log.v(httpTAG, " ID: " + result);
    }
    */

    // want 1 async task that gets called to GET several id codes
    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {

        String response = null;
        JSONObject mainObject =null;
        String id = null;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(GetGuidsActivity.this);
            dialog.setTitle("Working on downloading IDs...");
            dialog.setMessage("Please wait...");
            dialog.setIndeterminate(true);
            dialog.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            String msg = ("PARAMS"+ params.toString());
            Log.v("GetGuidsAc", msg);
            return 0;
        }
        protected Integer doInBackground_OLD(String... params) {
            InputStream inputStream = null;
            HttpsURLConnection urlConnection = null;
            int statusCode = 0;

            // can loop n times through the process of getting ids
            for (int n=0;n<1;n++) {

                try {
                /* forming the java.net.URL object */
                    URL url = new URL(EndPointServerURL.cleveridclient + "getId");

                    urlConnection = (HttpsURLConnection) url.openConnection();

                /* optional request header */
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                /* optional request header */
                    urlConnection.setRequestProperty("Authorization", "Bearer " + oauth2Token);
                /* for Get request */
                    urlConnection.setRequestMethod("GET");
                    statusCode = urlConnection.getResponseCode();
                    Log.v(httpTAG, "Status code..." + statusCode + "\n");
                    Log.v(httpTAG, "\n" + "the email scope token is... " + oauth2Token + "\n");

                /* 200 represents HTTP OK */
                    if (statusCode == 200) {
                        inputStream = new BufferedInputStream(urlConnection.getInputStream());
                        response = convertInputStreamToString(inputStream);

                        Log.v(httpTAG, "\n" + "Response: " + response);
                        try {

                            mainObject = new JSONObject(response);

                            id = mainObject.getString("id");

                            // add id to list of Guid Strings = id;
                            someGuids.add(id);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        // for debugging I want to log the response here eg. 401
                        response = "0"; //"Failed to fetch data!";
                    }
                } catch (Exception e) {
                    Log.d(httpTAG, e.getLocalizedMessage());
                }

            } // end of for loop

            return statusCode; //"Failed to fetch data!";

        } // end of doInBackground

        @Override
        protected void onPostExecute(Integer result) {
        /* Download complete. Lets update a file */

            // see http://developer.android.com/guide/topics/data/data-storage.html#filesInternal
            // FileOutputStream fos;
            // String newLine = "\n";

            if (result == 200) {

               try {
//                   fos = openFileOutput(FILENAME, getApplicationContext().MODE_PRIVATE);
//                   for (String id:someGuids) {
//                       fos.write(id.getBytes());
//                       // put a \n after each id
//                       fos.write(newLine.getBytes());
//                   }
//                       fos.close();

                    // saving the GUID in shared prefs
                    // using default shared pref file
                   //
                   //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

                   Context context = getApplicationContext();
                   // using shared prefs stored in filename
                   /*
                   SharedPreferences prefs = new SecurePreferences(context, "password", FILENAME);

                   SharedPreferences.Editor editor = prefs.edit();
                   for (String id:someGuids) {
                       editor.putString("id",id).apply();

                       Log.v(httpTAG, " ID: " + id);
                       // log encrypted file contents to check it is encrypted...
                       // this is done over in QRFragment

                   }
                   */


               } catch (Exception e) {
                   Log.d(httpTAG, e.getLocalizedMessage());

               }


            } else {
                Log.e(httpTAG, "Failed to write IDs to file!");
            }


            dialog.dismiss();

            //Since API level 11 (Honeycomb), you can call the recreate() method of the activity (thanks to this answer).

            //The recreate() method acts just like a configuration change, so your onSaveInstanceState() and onRestoreInstanceState() methods are also called, if applicable
            //but I need the QRFragment reloaded after getting a new GUID

            /*
            Intent  intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
            */
            Intent mainActivityIntent = new Intent();
            mainActivityIntent.putExtra(GUID_RESULT, "gotten from GetGuidsActivity");
            setResult(RESULT_OK, mainActivityIntent);
            finish();
        }


        private String convertInputStreamToString(InputStream inputStream) throws IOException {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while ((line = bufferedReader.readLine()) != null) {
                result += line;

            }

        /* Close Stream */
            if (null != inputStream) {
                inputStream.close();
            }
            return result;
        }

    } //end of class AsyncHttpTask


} // end of GetGuidsActivity
