package net.cleverid.android;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;

import net.cleverid.backend.cleverid.Cleverid;

import java.io.IOException;
import java.util.List;

/**
 * Created by denz on 11/28/15.
 */
public class GuidService extends IntentService {
    private static final String TAG = GuidService.class.getCanonicalName();
    private static Cleverid myApiService = null;
    private String email = null;
    public static GoogleCredential credential = null;
    private String oauth2Token = null;
    private static final String googleSCOPE =
            "oauth2:https://www.googleapis.com/auth/userinfo.email";

    public GuidService(String name) {
        super(name);
    }
    public GuidService() {
        this("GuidService");
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        oauth2Token = intent.getStringExtra("token");
        initApi();
        super.onStartCommand(intent, flags, startId);
        /*
        if (email != null) {
            //initTokenAndApi();
        } else {
            String message = "No email in settings! Please supply email address and try again!";
            Log.e(TAG, message);
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        }
        */
        return START_STICKY;
    }
    private void initTokenAndApi() {
        renewOauth2Token();
        initApi();
    }
    private void initApi() {
        credential = new GoogleCredential().setAccessToken(oauth2Token);
        Cleverid.Builder builder = new Cleverid.Builder(AndroidHttp.newCompatibleTransport(),
                new AndroidJsonFactory(), credential)
                // options for running against local devappserver
                // - 10.0.2.2 is localhost's IP address in Android emulator
                // - turn off compression when running against local devappserver
                .setRootUrl(EndPointServerURL.cleverid);
        // See https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloEndpoints
        // end options for devappserver

        myApiService = builder.build();
    }

    private void renewOauth2Token() {
        try {
            oauth2Token = GoogleAuthUtil.getToken(this, email, googleSCOPE);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        } catch (UserRecoverableAuthException e) {
            //mainActivity.startActivityForResult(e.getIntent(), REQ_SIGN_IN_REQUIRED);
        } catch (GoogleAuthException e) {
            Log.e(TAG, e.getMessage());
        }
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        work();
    }
    protected void work() {
        int i = 0;
        if (oauth2Token == null) {
            //renewOauth2Token();
        }
        //SharedPreferences preferences = this.getSharedPreferences(Constants.PRIVATE_PREFERENCES, Context.MODE_PRIVATE);
        //SharedPreferences.Editor editor = preferences.edit();
        GuidModel guidModel = new GuidModel(this);
        while (true) {
            try {
                List<String> guids = myApiService.getIds(10).execute().getIds();
                Log.v(TAG, "GuidIntentService works! "+ guids);
                //editor.putString("guids", guids.toString());
                //editor.commit();
                guidModel.write(guids);
                Thread.sleep(5000); // allow 10 seconds for the request
            } catch (GoogleJsonResponseException e) {
                if (e.getDetails().getCode() == 401) {
                    // handle 401 = Unauthorized
                    Log.e(TAG, "401 Unauthorized "+ e.getMessage());
                    //initTokenAndApi();
                }
                stopSelf();
                break;
            } catch (InterruptedException e) {
                Log.e(TAG, e.getMessage());
                stopSelf();
                break;
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
                stopSelf();
                break;
            }
        }
    }
}
