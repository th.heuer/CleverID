package net.cleverid.android.receivers;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by denz on 11/27/15.
 */
public class PeriodicBackgroundTaskGuidSync {
    private final Application app;

    public PeriodicBackgroundTaskGuidSync(Application myApplication) {
        this.app = myApplication;

    }
    public static PeriodicBackgroundTaskGuidSync instance(Application myApplication) {
        return new PeriodicBackgroundTaskGuidSync(myApplication);
    }

    public void sync() {
        Toast.makeText(app, "Wake up call!", Toast.LENGTH_LONG).show();
        Log.v("WAKE", "Up Call from CleverID");
    }
}
