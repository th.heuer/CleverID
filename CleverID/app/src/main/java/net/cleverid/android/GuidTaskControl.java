package net.cleverid.android;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;

import net.cleverid.backend.cleverid.Cleverid;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by denz on 12/1/15.
 */
class GuidTaskControl {
    private final ScheduledExecutorService scheduler =
            Executors.newScheduledThreadPool(1);
    private final ScheduledExecutorService stopScheduler =
            Executors.newScheduledThreadPool(1);
    private static Cleverid myApiService = null;
    private String email = null;
    public static GoogleCredential credential = null;
    private String oauth2Token = null;
    private static final String googleSCOPE =
            "oauth2:https://www.googleapis.com/auth/userinfo.email";
    final State state = new State();
    private static final String TAG = GuidTaskControl.class.getCanonicalName();

    private Context mainActivity = null;
    private static final int GUID_AMOUNT = 20;
    private static final int REFRESH_RATE = GUID_AMOUNT * 5; // one GUID per 5 seconds
    private ScheduledFuture guidGetterHandle = null;
    private int updateFreq = 5;
    private ScheduledFuture<?> stopSchedulerHandle = null;

    public GuidTaskControl(Context mainActivity) {
        this.mainActivity = mainActivity;
    }

    public static Cleverid getMyApiService() {
        return myApiService;
    }

    public void setOauth2Token(String oauth2Token) {
        this.oauth2Token = oauth2Token;
    }

    public void initApi() {
        credential = new GoogleCredential().setAccessToken(oauth2Token);
        Cleverid.Builder builder = new Cleverid.Builder(AndroidHttp.newCompatibleTransport(),
                new AndroidJsonFactory(), credential)
                // options for running against local devappserver
                // - 10.0.2.2 is localhost's IP address in Android emulator
                // - turn off compression when running against local devappserver
                .setRootUrl(EndPointServerURL.cleverid);
        // See https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloEndpoints
        // end options for devappserver

        myApiService = builder.build();
    }
    private static class State {
        public boolean kill = false;
    }
    public void getGuidsFromInternet() {
        // this is run every 10 seconds (see below)
        if (getMyApiService() == null) {
            initApi();
        }
        try {
            List<String> guids = getMyApiService().getIds(20).execute().getIds();
            GuidModel model = new GuidModel(mainActivity);
            model.write(guids);
        } catch (GoogleJsonResponseException e) {
            if (e.getDetails().getCode() == 401) {
                // handle 401 = Unauthorized
                Log.e(TAG, "401 Unauthorized " + e.getMessage());
                state.kill = true;
                initTokenAndApi();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        checkInterval();
    }
    public void doUntilTokenInvalid() {
        final Runnable guidGetter = new Runnable() {
            public void run() {
                getGuidsFromInternet();
            }
        };
        updateFreq = Preferences.getInt("updateFreq", 5);
        int refreshRate = GUID_AMOUNT * updateFreq;
        final ScheduledFuture handle =
                scheduler.scheduleAtFixedRate(guidGetter, 0, refreshRate, TimeUnit.SECONDS);
        this.guidGetterHandle = handle;

        final Runnable stopper = new Runnable() {
            @Override
            public void run() {
                if (state.kill) {
                    handle.cancel(true);
                }
            }
        };
        stopSchedulerHandle = stopScheduler.schedule(stopper, refreshRate, TimeUnit.SECONDS);
    }

    /**
     * This checks whether the update frequency changed.
     * If it did, cancel all scheduled tasks and renew them with the new update frequency.
     */
    private void checkInterval() {
        int newFreq = Preferences.getInt("updateFreq", 5);
        if (newFreq != updateFreq) {
            guidGetterHandle.cancel(false);
            stopSchedulerHandle.cancel(false);
            doUntilTokenInvalid();
        }
    }

    private void initTokenAndApi() {
        SharedPreferences preferences = mainActivity.getSharedPreferences(
                Constants.PRIVATE_PREFERENCES, Context.MODE_PRIVATE);
        String email = preferences.getString("account", null);
        Oauth2TokenTask task = new Oauth2TokenTask(mainActivity);
        task.execute(email);
    }

    public ScheduledFuture getGuidGetterHandle() {
        return guidGetterHandle;
    }
}
