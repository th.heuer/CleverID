/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.cleverid.backend;

/**
 *
 * @author denz
 */
public class ClientIds {

	// change for web client

	public static final String WEB_CLIENT_ID =
			//"511690246358-g7ob0qlm6ltb266fkan687jkpbossuft.apps.googleusercontent.com";
			"968192447621-r2bilke9dm21hef96avussbse57cr6c2.apps.googleusercontent.com";
	//
	// Have to change this for my android app
	//

	public static final String ANDROID_CLIENT_ID =
            //"511690246358-gd581rlvtndi7iks709lj0c43ei0pjtd.apps.googleusercontent.com";
			"511690246358-2g8qmtluhhg7e83bpo1mt1ilu0mu6hfh.apps.googleusercontent.com"; // Tim's
			//"968192447621-s05p9b1hars8ur6cs7joooj41lmi6o5o.apps.googleusercontent.com"; // John's

	public static final String IOS_CLIENT_ID =
            "replace this with your iOS client ID";
	public static final String ANDROID_AUDIENCE = WEB_CLIENT_ID;

	public static final String EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email";

	// for testing the API on google API explorer
	public static final String API_EXPLORER_CLIENT_ID
			= com.google.api.server.spi.Constant.API_EXPLORER_CLIENT_ID;	
}
